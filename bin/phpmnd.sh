#!/bin/sh

set -eu

vendor/bin/phpmnd \
    --extensions=all \
    --allow-array-mapping \
    --include-numeric-string \
    --hint \
    --progress \
    --non-zero-exit-on-violation \
    ./
