#!/bin/sh

set -eu

vendor/bin/phpstan analyse -c phpstan.neon --level max ./
