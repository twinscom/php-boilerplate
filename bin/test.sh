#!/bin/sh

set -eu

SCRIPTS_DIR=$(dirname "$0")

"$SCRIPTS_DIR/php-cs-fixer/check.sh"
"$SCRIPTS_DIR/phpcf.sh"
"$SCRIPTS_DIR/pahout.sh"
"$SCRIPTS_DIR/psalm.sh"
"$SCRIPTS_DIR/phpstan.sh"
"$SCRIPTS_DIR/phan.sh"
"$SCRIPTS_DIR/phpmd.sh"
"$SCRIPTS_DIR/phpmnd.sh"
"$SCRIPTS_DIR/phpunit.sh" "$@"
