# php-boilerplate

A boilerplate for PHP packages

[![build status](https://gitlab.com/twinscom/php-boilerplate/badges/master/build.svg)](https://gitlab.com/twinscom/php-boilerplate/builds)
[![coverage report](https://gitlab.com/twinscom/php-boilerplate/badges/master/coverage.svg)](https://gitlab.com/twinscom/php-boilerplate/builds)

## Development

### Install

```sh
docker-compose run --rm php composer install
```

### Test

```sh
docker-compose run --rm php bin/test.sh
```

### Fix the code with PHP-CS-Fixer

```sh
docker-compose run --rm php bin/php-cs-fixer/fix.sh
```

## License

[CC0-1.0](LICENSE)
