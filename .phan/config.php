<?php

declare(strict_types=1);

return [
    'directory_list' => ['./'],

    'exclude_analysis_directory_list' => [
        'vendor/',
    ],

    'exclude_file_regex' => '@vendor/.*/(tests?|Tests?)/@',

    'strict_param_checking' => true,
    'strict_property_checking' => true,
    'strict_return_checking' => true,
    'guess_unknown_parameter_type_using_default' => true,
    'unused_variable_detection' => true,
    'enable_include_path_checks' => true,
    'warn_about_relative_include_statement' => true,

    'ignore_undeclared_variables_in_global_scope' => true,

    'plugins' => [
        'AlwaysReturnPlugin',
        'DemoPlugin',
        'DollarDollarPlugin',
        'DuplicateArrayKeyPlugin',
        'DuplicateExpressionPlugin',
        'InvalidVariableIssetPlugin',
        'InvokePHPNativeSyntaxCheckPlugin',
        'NonBoolBranchPlugin',
        'NonBoolInLogicalArithPlugin',
        'PregRegexCheckerPlugin',
        'PrintfCheckerPlugin',
        'SleepCheckerPlugin',
        'UnknownElementTypePlugin',
        'UnreachableCodePlugin',
        'UnusedSuppressionPlugin',
    ],
];
